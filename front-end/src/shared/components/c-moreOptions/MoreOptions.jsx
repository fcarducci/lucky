import React, {useState} from "react";
import "./MoreOptions.scss";
import firebase from "firebase"
import {Redirect, useHistory} from "react-router";


function MoreOptions() {

    const [loggedOut, setLoggedOut] = useState(false);
    const history=useHistory()
    const logOut=()=>{
        history.push("/access")
    }
    return (
        <section className="c-more-options">
            <div className="c-more-options__container1">
                <p className="c-more-options__item">
                    <img className="c-more-options__img" src={process.env.PUBLIC_URL + "/assets/images/protectora.png"}
                         alt="protectora"/>
                    <span className="c-more-options__text">Asociaciones protectoras</span>
                    <img className="c-more-options__arrow"
                         src={process.env.PUBLIC_URL + "/assets/images/arrow_right_pink.png"} alt="arrow_right_pink"/>
                </p>
                <p className="c-more-options__item">
                    <img className="c-more-options__img" src={process.env.PUBLIC_URL + "/assets/images/eventos.png"}
                         alt="eventos"/>
                    <span className="c-more-options__text">Eventos</span>
                    <img className="c-more-options__arrow"
                         src={process.env.PUBLIC_URL + "/assets/images/arrow_right_pink.png"} alt=""/>
                </p>
                <p className="c-more-options__item">
                    <img className="c-more-options__img" src={process.env.PUBLIC_URL + "/assets/images/blogCopy.png"}
                         alt=""/>
                    <span className="c-more-options__text">Curiosidades</span>
                    <img className="c-more-options__arrow"
                         src={process.env.PUBLIC_URL + "/assets/images/arrow_right_pink.png"} alt=""/>
                </p>
                <p className="c-more-options__item">
                    <img className="c-more-options__img" src={process.env.PUBLIC_URL + "/assets/images/ayuda_rosa.png"}
                         alt=""/>
                    <span className="c-more-options__text">Ayuda</span>
                    <img className="c-more-options__arrow"
                         src={process.env.PUBLIC_URL + "/assets/images/arrow_right_pink.png"} alt=""/>
                </p>
                <p className="c-more-options__item">
                    <img className="c-more-options__img" src={process.env.PUBLIC_URL + "/assets/images/confi.png"}
                         alt=""/>
                    <span className="c-more-options__text">Configuración</span>
                    <img className="c-more-options__arrow"
                         src={process.env.PUBLIC_URL + "/assets/images/arrow_right_pink.png"} alt=""/>
                </p>
            </div>

            <div className="c-more-options__container2">
                <p className="c-more-options__item" onClick={() => {
                    if (JSON.parse(localStorage.getItem('lucky-token'))) {
                        localStorage.removeItem('lucky-token');
                        localStorage.removeItem('lucky-user');
                        //setLoggedOut(true);
                        logOut()
                    } else {
                        firebase.auth().signOut();
                        localStorage.removeItem('firebaseui::rememberedAccounts');
                    }
                }}>
                    <img className="c-more-options__img" src={process.env.PUBLIC_URL + "/assets/images/salir.png"}
                         alt=""/>
                    <span className="c-more-options__text">Cerrar sesión</span>
                </p>
            </div>
            {/*{loggedOut && logOut()}*/}
        </section>
    )
}

export default MoreOptions
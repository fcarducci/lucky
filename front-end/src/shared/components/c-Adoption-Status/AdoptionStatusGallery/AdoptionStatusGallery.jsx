import React from "react";
import {Link} from "react-router-dom";
import "./AdoptionStatusGallery.scss"

function AdoptionStatusGallery(props) {
    const url ="/adoption-details/";

    return (
        <div className="c-adoption-status-gallery">

            {props.myAnimals.map((item, index) =>
                <Link className="c-adoption-status-gallery__link-status" to={`${url}${item.id}/${props.adoptionApplies[index].id}`}>
                    <div className="c-adoption-status-gallery__container" key={index}>

                        <div className="c-adoption-status-gallery__title-status">
                            <p className="c-adoption-status-gallery__title">Adopcion de {item.name}</p>
                            {item.adoptionStatus === "En proceso" &&
                            <p className="c-adoption-status-gallery__status c-adoption-status-gallery__status--yellow">{item.adoptionStatus}
                                <img className="c-adoption-status-gallery__status-img"
                                     src={process.env.PUBLIC_URL + "./assets/images/ovalyellow.png"}/></p>}
                            {item.adoptionStatus === "Rechazado" &&
                            <p className="c-adoption-status-gallery__status c-adoption-status-gallery__status--red">{item.adoptionStatus}
                                <img className="c-adoption-status-gallery__status-img"
                                     src={process.env.PUBLIC_URL + "./assets/images/ovalred.png"}/></p>}
                            {item.adoptionStatus === "Completado" &&
                            <p className="c-adoption-status-gallery__status c-adoption-status-gallery__status--green">{item.adoptionStatus}<img
                                className="c-adoption-status-gallery__status-img c-adoption-status-gallery__status-img--green"
                                src={process.env.PUBLIC_URL + "./assets/images/Punto_verde.png"}/></p>}

                        </div>


                        <div className="c-adoption-status-gallery__location">
                            <img className="c-adoption-status-gallery__img" src={item.imageUrl}/>
                            <div className="c-adoption-status-gallery__data">
                                <table className="c-adoption-status-gallery__table">
                                    <tr className="c-adoption-status-gallery__city">
                                        <td>Nombre</td>
                                        <td className="c-adoption-status-gallery__value">{item.name}</td>
                                    </tr>
                                    <tr className="c-adoption-status-gallery__city">
                                        <td>Ciudad</td>
                                        <td className="c-adoption-status-gallery__value">{item.city}</td>
                                    </tr>
                                    <tr className="c-adoption-status-gallery__city">
                                        <td>Sexo</td>
                                        <td className="c-adoption-status-gallery__value">{item.sex}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </Link>
            )}

        </div>
    )
}

export default AdoptionStatusGallery
import React, {useState} from 'react';
import "./MapsFilter.scss"
import {Dialog} from 'primereact/dialog';
import {Link} from "react-router-dom";
import {useForm} from "react-hook-form";
import filtrosAll from "../c-AdoptionFilter/filters";


function MapsFilter(props) {

    const [visible, setVisible] = useState(false);
    const filtrosMap = [
        {
            img: process.env.PUBLIC_URL + './assets/img/Veterinario.png',
            imgColor: process.env.PUBLIC_URL + './assets/img/Veterinario_amarillo.png',
            filtered: false,
            name: "Veterinarios"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Peluqueria.png',
            imgColor: process.env.PUBLIC_URL + './assets/img/Peluqueria_morado.png',
            filtered: false,
            name: "Peluquerías"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Pet_friendly.png',
            imgColor: process.env.PUBLIC_URL + './assets/img/PetFriendly_verde.png',
            filtered: false,
            name: "Pet Friendly"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Educacion.png',
            imgColor: process.env.PUBLIC_URL + './assets/img/Educacion_azul.png',
            filtered: false,
            name: "Educación"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Guarderia.png',
            imgColor: process.env.PUBLIC_URL + './assets/img/Guarderia_magenta.png',
            filtered: false,
            name: "Guardería"
        },
        {
            img: process.env.PUBLIC_URL + './assets/img/Tienda.png',
            imgColor: process.env.PUBLIC_URL + './assets/img/Tienda_naranja.png',
            filtered: false,
            name: "Tienda"
        }
    ]

    let filtrosAplicados = {
        services: [],
    };

    function servicesFilter(e, filtros, index) {
        if (filtrosMap[index].filtered) {
            filtrosMap[index].filtered = false;
            e.target.src = filtrosMap[index].img;
            filtrosAplicados["services"] = filtrosAplicados["services"].filter(item => item !== filtros);
            console.log(filtrosAplicados);
        } else {
            filtrosMap[index].filtered = true;
            e.target.src = filtrosMap[index].imgColor;
            filtrosAplicados["services"].push(filtros);
            console.log(filtrosAplicados);
        }
    }

    const sendData= ()=>{
        props.filters(filtrosAplicados)
        console.log(filtrosAplicados)
    }

    return (

        <div>

            <Dialog header="" visible={visible} modal={true} onHide={() => setVisible(false)}>
                <div className="c-filter">
                    <div className="c-filter__container">
                        <p className="c-filter__title">Filtros</p>
                        <div className="row">
                            {filtrosMap.map((item, index) =>
                                <div className="col-4" key={index}>
                                    <div className="c-filter__item">
                                        <img onClick={(e) => servicesFilter(e, item.name, index)}
                                             className="c-filter__img" src={item.img} alt="item.title"/>
                                    </div>
                                    <br/>
                                </div>
                            )}
                        </div>
                        <div className="c-filter__button">

                            <button onClick={(e) => {
                                setVisible(false)
                               return  sendData()
                            }} className="c-filter__btn">Aplicar</button>

                        </div>
                    </div>
                </div>
            </Dialog>

            <div className="c-filter-maps" onClick={(e) => setVisible(true)}>

                <img className="c-filter-maps__img" src={process.env.PUBLIC_URL + "./assets/img/filtros.png"} alt=""/>

            </div>
        </div>

    )
}

export default MapsFilter
import React, {useContext} from "react";
import {Link, useLocation} from "react-router-dom";
import firebase from "firebase"
import "./Footer.scss"
import UserContext from "../../contexts/UserContext";


function Footer() {
    const location = useLocation();
    const [user, setUser] = useContext(UserContext);

    return (
        <footer className="c-footer">
            <Link to="/home">
                {location.pathname === "/home"
                    ? <img className="c-footer__img" src={process.env.PUBLIC_URL + "/assets/images/home_red.png"}
                           alt="Home"/>
                    : <img className="c-footer__img" src={process.env.PUBLIC_URL + "/assets/images/home_blue.png"}
                           alt="Home"/>}
            </Link>
            <Link to="/maps">

                {location.pathname === "/maps"
                    ? <img className="c-footer__img" src={process.env.PUBLIC_URL + "/assets/images/mapa_red.png"}
                           alt="Mapa"/>
                    : <img className="c-footer__img"
                           src={process.env.PUBLIC_URL + "/assets/images/mapa_blue.png"} alt="Mapa"/>}

            </Link>
            <Link to="/adoption">

                {location.pathname === "/adoption"
                    ? <img className="c-footer__img" src={process.env.PUBLIC_URL + "/assets/images/mascota_red.png"}
                           alt="Mascota"/>
                    : <img className="c-footer__img" src={process.env.PUBLIC_URL + "/assets/images/mascota_blue1.png"}
                           alt="Mascota"/>}

            </Link>
            <Link to="/userprofile">

                {location.pathname === "/userprofile"
                    ? <img className="c-footer__img c-footer__img__profile-red"
                           src={user.imageUrl ? user.imageUrl : firebase.auth().currentUser && firebase.auth().currentUser.photoURL} alt="Perfil"/>
                    : <img className="c-footer__img c-footer__img__profile-blue"
                           src={user.imageUrl ? user.imageUrl : firebase.auth().currentUser && firebase.auth().currentUser.photoURL} alt="Perfil"/>}

            </Link>
            <Link to="/more">

                {location.pathname === "/more"
                    ? <img className="c-footer__img" src={process.env.PUBLIC_URL + "/assets/images/mS_red.png"}
                           alt="Más"/>
                    : <img className="c-footer__img" src={process.env.PUBLIC_URL + "/assets/images/mS_blue.png"}
                           alt="Más"/>
                }
            </Link>
        </footer>
    )
}

export default Footer
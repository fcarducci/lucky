import React, {useState} from "react";
import {useForm} from "react-hook-form";
import "./AdoptionInput.scss";
function AdoptionInput(props){

    const {register, handleSubmit} = useForm();


    const onSubmit = data => {
        props.onSearch(data)

    };

    return(
        <form className="p-adoption__input-container">
            <input ref={register} name="search" className="p-adoption__input" type="text"
                   placeholder="Buscar"/>
            <img onClick={handleSubmit(onSubmit)} className="p-adoption__search"
                         src={process.env.PUBLIC_URL + "./assets/Adoption/images/buscar.png"}/>
        </form>
    )
}
export default AdoptionInput
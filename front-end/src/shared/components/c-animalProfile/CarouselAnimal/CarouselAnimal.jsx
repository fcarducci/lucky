import React from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import {Carousel} from 'react-responsive-carousel';
import "./CarouselAnimal.scss";

function CarouselAnimal(props) {
    return (
        <Carousel className="c-carousel-animal">
            <div className="c-carousel-animal__container">
                <img className="p-animal-profile__img"
                     src={props.animalImage} alt="img"/>
            </div>
        </Carousel>
    )
}

export default CarouselAnimal
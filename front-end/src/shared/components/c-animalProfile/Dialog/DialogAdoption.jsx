import React, {useState} from "react";
import {Dialog} from "primereact/dialog";
import {Button} from 'primereact/button';
import {Link} from "react-router-dom";
import "./DialogAdoption.scss"


function DialogAdoption() {
    const [displayBasic, setDisplayBasic] = useState(false);
    const [position, setPosition] = useState('center');

    const onClick = (stateMethod, position = '') => {
        stateMethod(true);

        if (position) {
            setPosition(position);
        }
    }

    const onHide = (stateMethod) => {
        stateMethod(false);
    }

    const renderFooter = (stateMethod) => {
        return (
            <div>
                <Button label="Cancelar"  onClick={() => onHide(stateMethod)} />
                <Link to="/formadoption">  <Button label="Continuar"  onClick={() => onHide(stateMethod)} className="p-button-secondary"/></Link>
            </div>
        );
    }

    return (
        <div className="c-dialog-animal">
            <button onClick={() => onClick(setDisplayBasic)} className="c-dialog-animal__button">Adoptar</button>
            <Dialog className="c-dialog-animal__content" header="Solicitud de adopción" visible={displayBasic} style={{width: '50vw'}} onHide={() => onHide(setDisplayBasic)} footer={renderFooter(setDisplayBasic)}>
                <p className="c-dialog-animal__desc">Adoptar es un acto de amor, pero
                    sobre todo una responsabilidad
                    de por vida.
                </p>
                <p className="c-dialog-animal__desc">Por éste motivo es importante que veas el siguiente vídeo donde te explicamos como va a ser todo el proceso de adopción.
                </p>
                <img className="c-dialog-animal__img" src={process.env.PUBLIC_URL + "/assets/images/group5.png"} alt="img"/>
                <p className="c-dialog-animal__question">¿Quieres continuar con el proceso
                    de adopción?</p>
            </Dialog>
        </div>
    )
}

export default DialogAdoption
import React, {useState} from "react";
import FormAdoptionComponent1 from "../../shared/components/c-formAdoption/Form1/FormAdoptionComponent1";
import FormAdoptionComponent2 from "../../shared/components/c-formAdoption/Form2/FormAdoptionComponent2";
import FormAdoptionComponent3 from "../../shared/components/c-formAdoption/Form3/FormAdoptionComponent3";
import axios from "axios";

function FormAdoption(){
    const [changeForm, setChangeForm]=useState(0)
    const [animalData, setAnimalData]=useState({})
    const handlerForm=(num)=>{
        setChangeForm(num)
    }

    const addAnimalData=(newAnimalData)=>{
        setAnimalData(Object.assign(animalData, newAnimalData))
         if(Object.keys(animalData).length > 18) {
        axios.post('http://localhost:3002/adoption', { animalData }).then(res=>{
            //Server response
        }).catch(e=>{
            console.log(e)
        })
        }
    }
    console.log(animalData)

    return(
        <div>
            {changeForm===0 && <FormAdoptionComponent1
                handlerForm={handlerForm}
                addAnimalData={addAnimalData}
            />}

            {changeForm===1 && <FormAdoptionComponent2
                handlerForm={handlerForm}
                addAnimalData={addAnimalData}
            />}

            {changeForm===2 && <FormAdoptionComponent3
                // handlerForm={handlerForm}
                addAnimalData={addAnimalData}
            />}

        </div>
    )
}

export default FormAdoption
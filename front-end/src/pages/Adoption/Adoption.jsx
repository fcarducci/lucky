import React, {useEffect, useState} from "react";
import AdoptionCarousel from "../../shared/components/c-adoptionPage/AdoptionCarousel/AdoptionCarousel";
import "./Adoption.scss"
import {Link} from "react-router-dom";
import AdoptionInput from "../../shared/components/c-adoptionPage/AdoptionInput/AdoptionInput";
import AdoptionGallery from "../../shared/components/c-adoptionPage/AdoptionGallery/AdoptionGallery";
import AdoptionGalleryFilter from "../../shared/components/c-adoptionPage/AdoptionGalleryFilter/AdoptionGalleryFilter";
import axios from "axios";
import AdoptionFilter from "../../shared/components/c-AdoptionFilter/AdoptionFilter";

function Adoption() {
    const [animals, setAnimals] = useState([])
    const [filteredAnimals, setFilteredAnimals] = useState([])
    const [toggleVisibility, settoggleVisibility] = useState(false)


    useEffect(() => {
        axios.get("http://localhost:3002/animal/all").then(res => {
            console.log(res)
            setAnimals(res.data.filter(item => !item.adopted))
            setFilteredAnimals(res.data.filter(item => !item.adopted))
        })
    }, []);


    const changeRender = (filters) => {
        const keys = ["city", "age", "sex", "size", "specie"];
        settoggleVisibility(toggleVisibility ? false : true);
        console.log(filters);
        console.log(animals);

        if (filters) {
            const getFiltered = (array, keys) => {
                return array.filter((item) => {
                    let valor = false;
                    for (let i = 0; i < keys.length; i++) {
                        valor = false;
                        if (Array.isArray(filters[keys[i]])) {
                            if ((filters[keys[i]].length === 0)) valor = true;
                            for (let j = 0; j < filters[keys[i]].length; j++) {
                                if ((item[keys[i]] === filters[keys[i]][j])) valor = true;
                            }
                            if (!valor) break;
                        } else {
                            if (item[keys[i]] === filters[keys[i]]) {
                                valor = true;
                            } else {
                                break;
                            }
                        }
                    }
                    return valor && item;
                })
            }

            setFilteredAnimals(getFiltered(animals, keys));
        }
    }

    const resetFilter=()=>{
        setFilteredAnimals(animals)
    }

    const formInput = (inputSearch) => {

        if (inputSearch !== "") {
            let listOfInputSearch = []
            listOfInputSearch = animals.filter(item => {
                    if (item.specie.toLowerCase().includes(inputSearch.search.toLowerCase())) {
                        return item
                    }
                }
            )

            console.log(listOfInputSearch)
            setFilteredAnimals(listOfInputSearch)
        }
    }



    return (
        <div className="p-adoption">

            {!toggleVisibility && (
                <>
                    <AdoptionInput
                        onSearch={formInput}
                    />

                    <p className="p-adoption__title">
                        Mis mascotas
                        <img className="p-adoption__mas"
                             src={process.env.PUBLIC_URL + "./assets/Adoption/images/mS.png"}/>
                    </p>
                    <p className="p-adoption__text">Accede al perfil de tus mascotas</p>
                    <AdoptionCarousel/>
                    <hr className="p-adoption__hr"/>

                    <Link to="/adoption-status" className="p-adoption__linkto-item-state">
                        <p className="p-adoption__item-state">
                            <span className="p-adoption__state">Estado de la adopción</span>
                            <img className="p-adoption__arrow"
                                 src={process.env.PUBLIC_URL + "/assets/images/arrow.png"}/>
                        </p>
                    </Link>
                    <p className="p-adoption__adoption">
                        Animales en adopción
                        <AdoptionFilter
                            changeRender={changeRender}
                        />
                    </p>
                    <AdoptionGallery
                        animals={filteredAnimals}

                    /></>)}
            {toggleVisibility && <AdoptionGalleryFilter
                animalsFilter={filteredAnimals}
                changeRender={changeRender}
                resetFilter={resetFilter}
            />}
        </div>
    )
}


export default Adoption;


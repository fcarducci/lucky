import React, {useContext, useState} from "react";
import './Home.scss'
import HomeCarousel from "../../shared/components/c-home/HomeCarousel/HomeCarousel";
import Novedades from "../../shared/components/c-home/Novedades/Novedades";
import UserContext from "../../shared/contexts/UserContext";



function Home() {
    const [user, setUser] = useContext(UserContext);
    return(
        <div className="p-home">
            <img className="p-home__logo" src={process.env.PUBLIC_URL + '/assets/images/logo.png'} alt=""/>
            <p className="p-home__greetings">{"¡Hola " + user.name  + "!"}</p>
            <HomeCarousel/>
            <hr className="p-home__hr"/>
            <Novedades/>
        </div>
    );
}

export default Home;
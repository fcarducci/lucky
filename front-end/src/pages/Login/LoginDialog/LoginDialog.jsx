import React, {useState} from "react";
import {Dialog} from "primereact/dialog";
import {useLocation} from "react-router-dom";
import {useHistory} from "react-router";
import "./LoginDialog.scss"

function LoginDialog(props) {
    const [displayBasic, setDisplayBasic] = useState(false);
    const [position, setPosition] = useState('center');
    const location = useLocation();

    const onClick = (stateMethod, position = '') => {
        stateMethod(true);

        if (position) {
            setPosition(position);
        }
    }

    const onHide = (stateMethod) => {
        stateMethod(false);
    }

    return (
        <div className="c-dialog-animal">
            <button type="button" className="c-login__button" onClick={() => onClick(setDisplayBasic)}>Iniciar con RRSS
            </button>
            <Dialog className="c-dialog-animal__content c-dialog-animal__content--rrss" header="Iniciar con RRSS" visible={displayBasic}
                    // style={{width: '50vw', heigth: "50vh"}}
                    onHide={() => onHide(setDisplayBasic)}>
                <img className="c-dialog-animal__cerrar" onClick={useHistory().go}
                     src={process.env.PUBLIC_URL + "/assets/images/cerrar.png"}
                     alt="cerrar"/>

                {props.logInFirebase}

            </Dialog>
        </div>
    )
}

export default LoginDialog;
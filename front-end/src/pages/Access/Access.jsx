import React from "react";
import './Access.scss';
import {Link} from 'react-router-dom';

function Access() {

    return (
        <div className="p-access">
            <div className="p-access__content-box">
                <p className="p-access__title">¿Cómo quieres entrar?</p>
                <Link to={"/login"}>
                    <button className="p-access__button">Usuario</button>
                </Link>
                <button className="p-access__button">Asociación protectora</button>
                <p className="p-access__later">Registrarse en otro momento</p>
            </div>
        </div>
    );
}

export default Access;
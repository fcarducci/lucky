const Association = require('../models/AssociationSchema');

const findOneAssociationByEmail = ((email)=> {
    const association = Association.findOne({"email": email}, (err, doc) => {
        console.log(!err ? doc : err);
    });
    return association;
});

const findOneAssociationById = ((id) => {
    const association = Association.findOne({"id": id}, (err, doc) => {
        console.log(!err ? doc : err);
    });
    return association;
});

const findAllAssociations = (() => {
    const associations = Association.find({}, (err, doc) => {
        console.log(!err ? doc : err);
    });
    return associations;
})

const insertOneAssociation = (data)=> {
    const newAssociation = new Association({
        email: data.email,
        password: data.password,
        cif: data.cif,
        phone: data.phone,
        name: data.name,
        imageUrl: data.imageUrl,
        longitude: data.longitude,
        latitude: data.latitude,
        street: data.street,
        zipCode: data.zipCode,
        city: data.city,
        animals: data.animals
    });
    newAssociation.save((err)=> {
        console.log(!err ?  "Successfuly added a new association!!" : err);
    });
}

const updateOneAssociation = async (data) => {

    try {
    const association = await findOneAssociationById(data.id);

        for( let prop in data) {
            data[prop] != undefined ? association[prop] = data[prop] : null;
        }
        association.save();
        return association;
    }
    catch (err) {
        console.log(err);
        throw err;
    }
};

const deleteOneAssociation = async (id) => {

    try {
        const result = await Association.remove({"id": id});
        console.log(result);
    }
    catch (err) {
        console.log(err);
        throw err;
    }
};

module.exports = {
    findOneAssociationByEmail,
    findOneAssociationById,
    findAllAssociations,
    insertOneAssociation,
    updateOneAssociation,
    deleteOneAssociation
};
const Adoption = require('../models/AdoptionSchema');

const findOneAdoptionById = ((id) => {
    const adoption = Adoption.findOne({"id": id}, (err, doc) => {
        console.log(!err ? doc : err);
    });
    return adoption;
});

const findAllAdoptions = (() => {
    const adoptions = Adoption.find({}, (err, doc) => {
        console.log(!err ? doc : err);
    });
    return adoptions;
});

const insertOneAdoption = (data) => {

    const newAdoption = new Adoption({
        userId: data.userId,
        animalId: data.animalId,
        adoptionStatus: data.adoptionStatus,
        fullName: data.fullName,
        email: data.email,
        phone: data.phone,
        dni: data.dni,
        address: data.address,
        zipCode: data.zipCode,
        city: data.city,
        otherAnimals: data.otherAnimals,
        whichAnimals: data.whichAnimals,
        friendlyWithOtherAnimals: data.friendlyWithOtherAnimals,
        whyAdopt: data.whyAdopt,
        animalNeeds: data.animalNeeds,
        animalExpenses: data.animalExpenses,
        animalFeeding: data.animalFeeding,
        typeOfHome: data.typeOfHome,
        rental: data.rental,
        allowAnimals: data.allowAnimals,
        movingSoon: data.movingSoon,
        garden: data.garden,
        roommates: data.roommates,
        allAgreeAdoption: data.allAgreeAdoption,
        visitAllowed: data.visitAllowed,
        imageUrl: data.imageUrl,
        payMethod: data.payMethod,
        date: data.date,
        time: data.time
    });
    newAdoption.save((err) => {
        console.log(!err ? "Successfuly added a new Adoption!!" : err);
    })
}

const updateOneAdoption = async (data) => {

    try {
        const adoption = await findOneAdoptionById(data.id);

        for (let prop in data) {
            console.log(prop + " - lleva: " + data[prop]);
            // if (prop !== "imageUrl") {
            //     // console.log("en las props normales")
            data[prop] != undefined ? adoption[prop] = data[prop] : null;
            // } else {
            //     // console.log("en imgUrl prop")
            //     adoption.imageUrl = adoption.imageUrl.concat(data.imageUrl);
            // }
        }
        adoption.save();
        return adoption;
    } catch (err) {
        console.log(err);
        throw err;
    }
};

const deleteOneAdoption = async (id) => {

    try {
        const result = await Adoption.remove({"id": id});
        console.log(result);
    } catch (err) {
        console.log(err);
        throw err;
    }
};

module.exports = {
    findOneAdoptionById,
    findAllAdoptions,
    insertOneAdoption,
    updateOneAdoption,
    deleteOneAdoption
};
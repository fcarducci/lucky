const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');

router.route('/all').get(userController.findAllusers);
router.route('/')
    .post(userController.addUser)
    .put(userController.updateUser)
    .delete(userController.deleteUser);

module.exports = router;

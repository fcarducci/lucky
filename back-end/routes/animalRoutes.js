const express = require('express');
const router = express.Router();

const animalController = require('../controllers/animalController');

router.route("/all").get(animalController.findAllAnimals)
router.route("/:id").get(animalController.findAnimal)
router.route("/").post(animalController.addAnimal)
    .put(animalController.updateAnimal)
    .delete(animalController.deleteAnimal);

module.exports = router;

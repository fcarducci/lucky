const crudOpinions = require('../lib/crudOpinion');

const findOneOpinion = async (req, res) => {
    try {
        const opinion = await crudOpinions.findOneOpinionByObjectId(req.params._id);
        res.send(opinion);
    } catch (err) {
        console.log(err);
    }
}

const findAllOpinions = async (req, res) => {
    try {
        const opinions = await crudOpinions.findAllOpinions();
        res.send(opinions);
    } catch (err) {
        console.log(err);
    }
}

const addOpinion = async (req, res) => {
    try {
        console.log("data que llega", req.body.data);
        const opinion = await crudOpinions.insertOneOpinion(req.body.data);
        res.send(opinion);
    } catch (err) {
        console.log(err);
    }
}

const updateOpinion = async (req, res) => {
    try {
        const opinion = await crudOpinions.updateOneOpinion(req.body);
        res.send(opinion);
    } catch (err) {
        console.log(err);
    }
}

const deleteOpinion = async (req, res) => {
    try {
        const opinion = await crudOpinions.deleteOneOpinion(req.body.id);
        res.send(opinion);
    } catch (err) {
        console.log(err);
    }
}

module.exports = {
    findOneOpinion,
    findAllOpinions,
    addOpinion,
    updateOpinion,
    deleteOpinion
};
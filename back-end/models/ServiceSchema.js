const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ServiceSchema = new Schema({
    id: {
        type: Number,
        index: true,
        unique: true,
        require: true
    },
    typeOfService: {
        type: String,
        enum: ['Veterinarios', 'Peluquerías', 'Pet Friendly', 'Educación', 'Guardería', 'Tienda'],
        require: true
    },
    name: {
        type: String,
        require: true
    },
    imageUrl: {
        type: String,
    },
    punctuation: {
        type: Number
    },
    longitude: {
        type: String
    },
    latitude: {
        type: String
    },
    street: {
        type: String
    },
    zipCode: {
        type: String
    },
    city: {
        type: String
    },
    testimonials: [{
        type: Schema.Types.ObjectId,
        ref: 'opinion'
    }],
    opinions: {
        type: Array
    }

});

ServiceSchema.pre('save', async function (next) {

    try {
        if (this.id == undefined) {
            let biggerId = -1;
            const list = await Service.find();
            for (let doc of list) {
                if (doc.id != undefined && biggerId < doc.id) {
                    biggerId = doc.id;
                }
            }
            this.id = ++biggerId;
        }
        next();
    } catch (err) {
        console.log(err);
    }
});

const Service = mongoose.model('service', ServiceSchema);
module.exports = Service;

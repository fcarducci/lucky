const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OpinionSchema = new Schema({
    id: {
        type: Number,
        index: true,
        unique: true,
        require: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    userImage: {
        type: String,
    },
    // serviceId: [{
    //     type: Schema.Types.ObjectId,
    //     ref: 'service'
    // }],
    text: {
        type: String,
        require: true
    },
    valoration: {
        type: Number,
        require: true
    }
});

OpinionSchema.pre('save', async function (next) {

    try {
        if (this.id == undefined) {
            let biggerId = -1;
            const list = await Opinion.find();
            for (let doc of list) {
                if (doc.id != undefined && biggerId < doc.id) {
                    biggerId = doc.id;
                }
            }
            this.id = ++biggerId;
        }
        next();
    } catch (err) {
        console.log(err);
    }
});

const Opinion = mongoose.model('opinion', OpinionSchema);
module.exports = Opinion;

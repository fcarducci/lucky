const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AdoptionSchema = new Schema({
    id: {
        type: Number,
        index: true,
        unique: true,
        require: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    },
    animalId: {
        type: Schema.Types.ObjectId,
        ref: 'animal'
    },
    adoptionStatus: {
        type: String,
        enum: ['En proceso', 'Completado', 'Rechazado'],
        default: "En proceso"
    },
    fullName: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true
    },
    phone: {
        type: String,
        require: true
    },
    dni: {
        type: String,
        require: true
    },
    address: {
        type: String,
        require: true
    },
    zipCode: {
        type: String,
        require: true
    },
    city: {
        type: String,
        require: true
    },
    otherAnimals: {
        type: Boolean,
        require: true
    },
    whichAnimals: {
        type: String,
    },
    friendlyWithOtherAnimals: {
        type: String,
    },
    whyAdopt: {
        type: String,
        require: true
    },
    animalNeeds: {
        type: String,
        require: true
    },
    animalExpenses: {
        type: String,
        require: true
    },
    animalFeeding: {
        type: String,
        require: true
    },
    typeOfHome: {
        type: String,
        require: true
    },
    rental: {
        type: Boolean,
        require: true
    },
    allowAnimals: {
        type: Boolean,
        require: true
    },
    movingSoon: {
        type: Boolean,
        require: true
    },
    garden: {
        type: Boolean,
        require: true
    },
    roommates: {
        type: Boolean,
        require: true
    },
    allAgreeAdoption: {
        type: Boolean,
        require: true
    },
    visitAllowed: {
        type: Boolean,
        require: true
    },
    imageUrl: {
        type: Array,
    },
    payMethod: {
        type: String,
    },
    date: {
        type: Date,
    },
    time: {
        type: String,
    }
});

AdoptionSchema.pre('save', async function (next) {

    try {
        if (this.id == undefined) {
            let biggerId = -1;
            const list = await Adoption.find();
            for (let doc of list) {
                if (doc.id != undefined && biggerId < doc.id) {
                    biggerId = doc.id;
                }
            }
            this.id = ++biggerId;
        }
        next();
    } catch (err) {
        console.log(err);
    }
});

const Adoption = mongoose.model('adoption', AdoptionSchema);
module.exports = Adoption
